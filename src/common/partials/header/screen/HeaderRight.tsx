import React from 'react';
import {StyleSheet, View,Text} from "react-native";
import {HeaderRightPropsInterface, HeaderRightStyleInterface} from "../interface/HeaderInterface";
import colors from "../../../../assets/styles/colors";
import IconButton from "../../iconbutton";
import layout from "../../../../assets/styles/layout";

const HeaderRight:React.FC<HeaderRightPropsInterface> = () => {
    return (
        <View style={[styles.header,layout.displayRow, layout.alignCenter]}>
            <View style={styles.headerIcon}>
            <IconButton
                iconName={"bookmark"}
                iconColor={colors.dark}
                iconSize={22}
                onPressAction={() => console.log('hello icon button 1 ')}
            />
            </View>
            <View style={styles.headerIcon}>
                <IconButton
                    iconName={"shopping-cart"}
                    iconColor={colors.dark}
                    iconSize={25}
                    onPressAction={() => console.log('hello icon button 2  ')}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create<HeaderRightStyleInterface>({
    header:{
        paddingHorizontal:20
    },
    headerIcon:{
        paddingHorizontal: 10
    }
})

export default HeaderRight;

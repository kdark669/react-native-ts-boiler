import React from 'react';
import {StyleSheet, View,Text} from "react-native";
import {HeaderLeftPropsInterface, HeaderLeftStyleInterface} from "../interface/HeaderInterface";
import layout from "../../../../assets/styles/layout";
import typography from "../../../../assets/styles/typography";

const HeaderLeft:React.FC<HeaderLeftPropsInterface> = (props) => {
    const {
        logoName
    } = props
    return (
        <View style={[styles.header,layout.displayRow, layout.alignCenter]}>
            <Text style={[styles.headerLeft,typography.title]}>{logoName}</Text>
        </View>
    );
};

const styles = StyleSheet.create<HeaderLeftStyleInterface>({
    header:{
        paddingHorizontal:20
    },
    headerLeft:{
    }
})

export default HeaderLeft;

import { TextStyle, ViewStyle} from "react-native";

export interface HeaderLeftPropsInterface {
    logoName?:string
}
export interface HeaderLeftStyleInterface{
    header:ViewStyle
    headerLeft:TextStyle
}

export interface HeaderRightPropsInterface {

}
export interface HeaderRightStyleInterface{
    header:ViewStyle
    headerIcon:ViewStyle
}

import React from 'react';
import {TouchableOpacity} from "react-native";
import MyIcon from "../myicon";

interface IconButtonPropsInterface {
    iconName:string
    iconColor?:string
    iconSize?:number
    onPressAction: () => void
}
const IconButton:React.FC<IconButtonPropsInterface> = (props) => {
    const {
        iconName,
        iconColor,
        iconSize,
        onPressAction
    } = props
    return (
        <TouchableOpacity
            onPress={onPressAction}
        >
            <MyIcon
                iconName={iconName}
                iconColor={iconColor}
                iconSize={iconSize}
            />
        </TouchableOpacity>
    );
};

export default IconButton;

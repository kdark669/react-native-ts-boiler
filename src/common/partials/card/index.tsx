import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ViewStyle
} from 'react-native'
import colors from "../../../assets/styles/colors";

interface  CardPropInterface {

}
interface  CardStyleInterface {
    card:ViewStyle
}
const Card:React.FC<CardPropInterface> = () => {
    return (
       <View style={styles.card}>
           <Text> I am card </Text>
       </View>
    );
};

const styles  = StyleSheet.create<CardStyleInterface>({
    card:{
        padding:13,
        height:"auto",
        backgroundColor:"red",
        borderRadius:10,
        shadowColor: colors.dark,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
})

export default Card;

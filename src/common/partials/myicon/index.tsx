import React from 'react';
import Icon from "react-native-vector-icons/FontAwesome";
Icon.loadFont()

interface MyIconPropsInterface {
    iconName: string
    iconColor?: string
    iconSize?: number
}

const MyIcon: React.FC<MyIconPropsInterface> = (props) => {
    const {
        iconName,
        iconColor,
        iconSize
    } = props
    return (
        <Icon
            name={iconName}
            color={iconColor ? iconColor : "black"}
            size={iconSize ? iconSize : 25}
        />
    );
};

export default MyIcon;

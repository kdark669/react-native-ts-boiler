import React from 'react';
import {
    View,
    Text, StyleSheet, SafeAreaView
} from 'react-native'
import {HomePropsInterface, HomeStylesInterface} from "./interface/homeInterface";
import layout from "../../../../assets/styles/layout";

const HomeScreen:React.FC<HomePropsInterface> = () => {
    return (
       <SafeAreaView style={layout.safeArea}>
           <View style={layout.safeAreaContent}>
               <Text> I am home screen </Text>
           </View>
       </SafeAreaView>
    );
};

const styles = StyleSheet.create<HomeStylesInterface>({

})

export default HomeScreen

import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import { CustomerStackNavigationInterface } from "./interface/CustomerNavigationInterface";

import HeaderRight from "../../../common/partials/header/screen/HeaderRight";
import HeaderLeft from "../../../common/partials/header/screen/HeaderLeft";
import CustomerBottomNavigation from "./CustomerBottomNavigation";

const Stack = createStackNavigator<CustomerStackNavigationInterface>();

const CustomerStack: React.FC = () => {
    return (
        <Stack.Navigator
            initialRouteName="CustomerLayout"
            screenOptions={{
                headerTransparent: true,
            }}
        >
            <Stack.Screen
                name="CustomerLayout"
                component={CustomerBottomNavigation}
                options={{
                    headerShown: true,
                    title: "",
                    headerRight: () => <HeaderRight/>,
                    headerLeft:() => <HeaderLeft logoName={"Woodland"}/>
                }}
            />
        </Stack.Navigator>
    )
}

export default CustomerStack;

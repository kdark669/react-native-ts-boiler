import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {SupplierBottomNavigationInterface} from "./interface/SupplierNavigationInterface";

import colors from "../../../assets/styles/colors";
import vars from "../../../assets/styles/vars";

import MyIcon from "../../../common/partials/myicon";
import SupplierHome from "../screens/home";


const Tab = createMaterialBottomTabNavigator<SupplierBottomNavigationInterface>();

const SupplierBottomNavigation:React.FC = () => {
    const ACTIVE_TAB_COLOR = colors.active
    const INACTIVE_TAB_COLOR = colors.light
    return (
        <>
            <Tab.Navigator
                initialRouteName="Home"
                shifting={true}
                labeled={true}
                activeColor={ACTIVE_TAB_COLOR}
                inactiveColor={INACTIVE_TAB_COLOR}
                barStyle={{
                    position: 'absolute',
                    backgroundColor: colors.dark,
                    borderTopLeftRadius: 30,
                    borderTopRightRadius: 30,
                    marginBottom:-10,
                    // borderWidth: 0.5,
                    // borderBottomWidth: 1,
                    overflow: 'hidden',
                }}
            >
                <Tab.Screen
                    name="Home"
                    component={SupplierHome}
                    options={{
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ focused }) =>
                            <MyIcon
                                iconName={"home"}
                                iconColor={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
                                iconSize={vars.iconSize}
                            />
                    }}
                />
                <Tab.Screen
                    name="Category"
                    component={SupplierHome}
                    options={{
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ focused }) =>
                            <MyIcon
                                iconName={"list"}
                                iconColor={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
                                iconSize={vars.iconSize}
                            />
                    }}
                />

            </Tab.Navigator>
        </>
    );
};

export default SupplierBottomNavigation;

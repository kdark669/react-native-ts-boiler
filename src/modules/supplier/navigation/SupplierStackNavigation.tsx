import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import { SupplierStackNavigationInterface } from "./interface/SupplierNavigationInterface";

import HeaderRight from "../../../common/partials/header/screen/HeaderRight";
import HeaderLeft from "../../../common/partials/header/screen/HeaderLeft";
import SupplierBottomNavigation from "./SupplierBottomNavigation";

const Stack = createStackNavigator<SupplierStackNavigationInterface>();

const SupplierStack: React.FC = () => {
    return (
        <Stack.Navigator
            initialRouteName="SupplierLayout"
            screenOptions={{
                headerTransparent: true,
            }}
        >
            <Stack.Screen
                name="SupplierLayout"
                component={SupplierBottomNavigation}
                options={{
                    headerShown: true,
                    title: "",
                    headerRight: () => <HeaderRight/>,
                    headerLeft:() => <HeaderLeft logoName={"Woodland"}/>
                }}
            />
        </Stack.Navigator>
    )
}

export default SupplierStack;
